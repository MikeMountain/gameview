import {Firebase} from './firebase.config';

export const environment = {
  production: true,
  firebase: Firebase
};
