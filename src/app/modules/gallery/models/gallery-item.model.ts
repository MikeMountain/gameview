// export class GalleryItemModel {
//   title: string;
//   url: string;
//   game: string;
//   gameRef: string;
//   createdBy: string;
//   dateCreated: string;
//
//   constructor(options?: any) {
//     this.title = options && options.title;
//     this.url = options && options.url;
//     this.game = options && options.game;
//     this.gameRef = options && options.gameRef;
//     this.createdBy = options && options.created_by;
//     this.dateCreated = options && options.date_created;
//   }
// }

class WpDatasetItem {
  title: string;
  image: string;
  categories: string[];
  caption?: string;

  constructor(options?: WpDatasetItem) {
    if (options) {
      Object.keys(options).forEach(key => {
        this[key] = options[key];
      });
    }
  }

}

export class GalleryItem {
  id: number;
  acf: WpDatasetItem;
  showInfo: boolean;

  constructor(options?: GalleryItem) {
    if (options) {
      this.id = options.id;
      this.acf = new WpDatasetItem(options.acf);
    }
  }
}

export class SingleGalleryItem {
  acf: WpDatasetItem;

  constructor(options?: SingleGalleryItem) {
    this.acf = options && new WpDatasetItem(options.acf) || new WpDatasetItem();
  }
}
