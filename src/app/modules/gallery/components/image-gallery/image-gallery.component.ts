import {Component, OnDestroy, OnInit} from '@angular/core';
import {GalleryService} from '../../services/gallery.service';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {GalleryItem} from '../../models/gallery-item.model';

@Component({
  selector: 'app-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.scss']
})
export class ImageGalleryComponent implements OnInit, OnDestroy {

  galleryImages: any[] = [];

  paramSubscription: Subscription;
  imageSubscription: Subscription;

  constructor(private galleryService: GalleryService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.paramSubscription = this.route.params.subscribe(params => {
      this.imageSubscription = this.galleryService.fetchImages(params.game).subscribe(images => {
        this.galleryImages = images.map(image => new GalleryItem(image));
        console.log(this.galleryImages);
      });
    });
  }

  ngOnDestroy() {
    this.paramSubscription.unsubscribe();
    if (this.imageSubscription) {
      this.imageSubscription.unsubscribe();
    }
  }

}
