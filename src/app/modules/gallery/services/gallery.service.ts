import {Injectable} from '@angular/core';
import {BaseHttpService} from '../../shared/services/base-http.service';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {GalleryItem} from '../models/gallery-item.model';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GalleryService extends BaseHttpService {

  GalleryItemsSrc = new BehaviorSubject<GalleryItem[]>([]);

  constructor(public httpClient: HttpClient) {
    super(httpClient);
  }

  fetchImages(gameTitle: string): Observable<GalleryItem[]> {
    const url = super._setUrl(gameTitle);
    super._get<GalleryItem[]>(url).pipe(
      catchError(err => {
        console.log(err);
        return of([]);
      })
    ).subscribe(galleryImages => {
      this.GalleryItemsSrc.next(galleryImages);
    });
    return this.GalleryItemsSrc.asObservable();
  }
}
