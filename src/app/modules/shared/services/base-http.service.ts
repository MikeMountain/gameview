import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BaseHttpService {

  // baseUrl = 'http://gameviewhost.test/wp-json/acf/v3';
  baseUrl = 'https://gameviewhost.000webhostapp.com/wp-json/acf/v3';

  httpOptions = {
    headers: undefined as HttpHeaders
  };

  constructor(public httpClient: HttpClient) {
  }

  _setUrl(partial: string): string {
    return `${this.baseUrl}/${partial}`;
  }

  _get<T>(url: string, options?: any): Observable<T> {
    return this.httpClient.get<T>(url, {headers: options}).pipe(
      take(1)
    );
  }

  _post<T>(url: string, body: any, options?: any): Observable<T> {
    return this.httpClient.post<T>(url, body, {headers: options}).pipe(
      take(1)
    );
  }

  _put<T>(url: string, body: any, options?: any): Observable<T> {
    return this.httpClient.put<T>(url, body, {headers: options}).pipe(
      take(1)
    );
  }

  _delete<T>(url: string, options?: any): Observable<T> {
    return this.httpClient.delete<T>(url, {headers: options}).pipe(
      take(1)
    );
  }
}
