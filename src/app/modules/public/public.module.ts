import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PublicRoutingModule} from './public-routing.module';
import {SharedModule} from '../shared/shared.module';
import {GalleryShellComponent} from './components/gallery-shell/gallery-shell.component';
import { HomeComponent } from './components/home/home.component';
import {GalleryModule} from '../gallery/gallery.module';

@NgModule({
  declarations: [
    GalleryShellComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    SharedModule,
    GalleryModule
  ]
})
export class PublicModule {
}
