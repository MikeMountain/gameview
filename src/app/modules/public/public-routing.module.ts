import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {GalleryShellComponent} from './components/gallery-shell/gallery-shell.component';
import {HomeComponent} from './components/home/home.component';
import {ImageGalleryComponent} from '../gallery/components/image-gallery/image-gallery.component';


const routes: Routes = [
  {
    path: '',
    component: GalleryShellComponent,
    children: [
      {path: 'home', component: HomeComponent},
      {path: 'images/:game', component: ImageGalleryComponent},
      {path: '', pathMatch: 'full', component: HomeComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule {
}
