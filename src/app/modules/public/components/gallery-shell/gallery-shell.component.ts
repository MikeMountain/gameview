import {Component, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';
import {GalleryService} from '../../../gallery/services/gallery.service';

@Component({
  selector: 'app-gallery-shell',
  templateUrl: './gallery-shell.component.html',
  styleUrls: ['./gallery-shell.component.scss']
})
export class GalleryShellComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,
              private galleryService: GalleryService) {
  }

  ngOnInit() {
  }

}
